package en.awacademy.game;

import javafx.animation.PauseTransition;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.FileInputStream;


public class Graphics {

    private Model model;
    private GraphicsContext gc;

    public Image imageTitle =  ImageLoader.loadImage(
            "C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\textures\\title.png");
    public Image imageCorrect =  ImageLoader.loadImage(
            "C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\textures\\correct.png");
    public Image imageIncorrect =  ImageLoader.loadImage(
            "C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\textures\\incorrect.png");

    public String roundNumber = "Round ";


    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }


    public void draw(){

        drawBoard();

        if(this.model.getPhase() == 0){
            drawInstructions();
        }

        //model phase = 1
        drawHighlight();


        if (this.model.getPhase() == 2){

            drawBoard();
            gc.setFill(Color.BLACK);
            gc.setFont(new Font(60.0));
            gc.fillText("GO", 210, 268);
        }

        //gc.setFont(new Font(12.0));
        //model phase = 2
        drawSmiley();

    }


    public void drawBoard(){
        gc.setFont(new Font(12.0));
        gc.setFill(Color.valueOf("#318fb2"));
        gc.fillRect(0, 0, Main.WIDTH, Main.HEIGHT);


        //not needed
        /*gc.setFill(Color.WHITE);
        gc.fillText("Animation Counter: " + model.getAnimationCounter(), 300, 50);*/

        gc.drawImage(this.imageTitle,10, 10);

        for (Square square : model.getFixedSquares()) {
            gc.setFill(Color.valueOf(square.getColor()));
            gc.fillRect(square.getPosX(), square.getPosY(), 100, 100);
        }

        gc.setFill(Color.BLACK); //Start button
        gc.fillRect(400, 400, 50, 50);
        gc.setFill(Color.WHITE); //Start button
        gc.fillText("START", 410, 430);
    }


    public void drawBoardWithoutSquares(){
        gc.setFill(Color.valueOf("#318fb2"));
        gc.fillRect(0, 0, Main.WIDTH, Main.HEIGHT);


        //not needed
        /*gc.setFill(Color.WHITE);
        gc.fillText("Animation Counter: " + model.getAnimationCounter(), 300, 50);*/

        gc.drawImage(this.imageTitle,10, 10);


        gc.setFill(Color.BLACK); //Start button
        gc.fillRect(400, 400, 50, 50);
        gc.setFill(Color.WHITE); //Start button
        gc.fillText("START", 410, 430);
    }



    public void drawInstructions(){
        gc.setFill(Color.BLACK);
        gc.setFont(new Font(20));
        gc.fillText("Instructions:", 35, 375);
        gc.setFill(Color.WHITE);
        gc.setFont(new Font(16));
        gc.fillText("1) Press start", 35, 400);
        gc.fillText("2) A square changes its color", 35, 420);
        gc.fillText("3) Remember it!", 35, 440);
        gc.fillText("4) Go! Click on the correct square", 35, 460);
        gc.fillText("5) Press start to go to next level", 35, 480);

    }



    public void drawHighlight(){
        if (this.model.getRandomSquares().size() > 0) {

            gc.setFill(Color.BLACK); //Round number
            gc.strokeText(roundNumber + this.model.getRandomSquares().size(), 230, 120);

            for (int i = 0; i < this.model.getRandomSquares().size(); i++){

                if(this.model.getAnimationCounter() > i * 550 && this.model.getAnimationCounter() < (i+1) * 500){
                    highlightColor(this.model.getRandomSquares().get(i));
                    //this.model.playSound(this.model.getRandomSquares().get(i).getSoundPath());

                }
            }
        }
    }





    public void drawSmiley(){
        gc.setFill(Color.BLACK);
        gc.setFont(new Font(40.0));

        if(this.model.getPhase() == 2){

            if(this.model.correctString.equals("Correct")){
                gc.drawImage(imageCorrect, 100, 400);
                gc.fillText(this.model.correctString, 190, 400);
            }

            if(this.model.correctString.equals("Incorrect")){
                gc.drawImage(imageIncorrect, 100, 400);
                gc.fillText(this.model.correctString, 170, 400);
            }
        }
    }

    public void highlightColor(Square square) {

        switch (square.getColor()) {
            case "#e39c96"://red
                gc.setFill(Color.valueOf("#eb200e"));
                gc.fillRect(square.getPosX(), square.getPosY(), 100, 100);
                break;
            case "#9cbae8"://blue
                gc.setFill(Color.valueOf("#0e6aeb"));
                gc.fillRect(square.getPosX(), square.getPosY(), 100, 100);
                break;
            case "#9ae3c5"://green
                gc.setFill(Color.valueOf("#0eeb8e"));
                gc.fillRect(square.getPosX(), square.getPosY(), 100, 100);
                break;
            case "#d9dbb8"://yellow
                gc.setFill(Color.valueOf("#f5ed0a"));
                gc.fillRect(square.getPosX(), square.getPosY(), 100, 100);
                break;
        }
    }
}
