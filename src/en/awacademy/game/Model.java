package en.awacademy.game;


import javafx.scene.canvas.GraphicsContext;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {

    private GraphicsContext gc;

    private int phase = 0;
    private long animationCounter = 0;

    //private boolean correctStatus; not needed anymore
    public int correctCounter = 0;
    public String correctString ="";

    //fixed square array to get random serie
    private Square red = new Square(150,150,"#e39c96",
            "C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\sounds\\do.wav");//red
    private Square blue = new Square(150,250,"#9cbae8",
            "C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\sounds\\re.wav");//blue
    private Square green = new Square(250,150,"#9ae3c5",
            "C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\sounds\\mi.wav");//green
    private Square yellow = new Square(250,250,"#d9dbb8",
            "C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\sounds\\fa.wav");//yellow
    private Square[] fixedSquares = {red, blue, green, yellow};

    public Sound redDo = new Sound("C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\sounds\\do.wav");
    public Sound blueRe = new Sound("C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\sounds\\re.wav");
    public Sound greenMi = new Sound("C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\sounds\\mi.wav");
    public Sound yellowFa = new Sound("C:\\Users\\Jose Rangel\\IdeaProjects\\W3_Jose_Says_Game\\res\\sounds\\fa.wav");


    private List<Square> randomSquares = new LinkedList<>();
    private List<String> userSquares = new LinkedList<>();

    public long getAnimationCounter() {
        return animationCounter;
    }

    public void setAnimationCounter(long animationCounter) {
        this.animationCounter = animationCounter;
    }


    public List<Square> getRandomSquares() {
        return randomSquares;
    }

    public void setRandomSquares(List<Square> randomSquares) {
        this.randomSquares = randomSquares;
    }

    public Square[] getFixedSquares() {
        return fixedSquares;
    }

    public List<String> getUserSquares() {
        return userSquares;
    }

    public void setUserSquares(List<String> userSquares) {
        this.userSquares = userSquares;
    }

    public int getPhase() {
        return phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

    public int getCorrectCounter() {
        return correctCounter;
    }

    public void setCorrectCounter(int correctCounter) {
        this.correctCounter = correctCounter;
    }

    public void newRandomSquare(){
        Random generator = new Random();
        int rndIndex = generator.nextInt(4);
        randomSquares.add(fixedSquares[rndIndex]);
    }

    public void newUserSquare(String color){
        userSquares.add(color);
    }

    public int countCorrectSquares(){

        if (getUserSquares().size() != 0){

            for (int i = 0; i < getUserSquares().size(); i++){
                if (getRandomSquares().get(i).getColor().equals(getUserSquares().get(i))){
                    correctCounter += 1;
                }
            }
        }
        return correctCounter;
    }

    public String compare(){
        if (correctCounter == getRandomSquares().size()){
            correctString = "Correct";
            System.out.println(correctCounter);
        } else {
            correctString = "Incorrect";
            System.out.println(correctCounter);
        }
        return correctString;
    }

    public void reset(){
        getUserSquares().clear();
        correctCounter = 0;
    }





    public void startShow() {
        phase = 1;
        animationCounter = 0;
    }

    public void waitForUser(){
        phase = 2;
    }

    public void update(long deltaMillis) {
        if(phase == 1) {
            animationCounter += deltaMillis;

            if (animationCounter/500 == getRandomSquares().size()){
                waitForUser();
            }
        }

        if (phase == 3){
            animationCounter += deltaMillis;
        }
    }

    public void playSound(String soundPath){
        Media sound = new Media(new File(soundPath).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(sound);
        mediaPlayer.play();
    }
}
