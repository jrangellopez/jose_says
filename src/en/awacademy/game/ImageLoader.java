package en.awacademy.game;

import javafx.scene.image.Image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ImageLoader {


    public static Image loadImage(String path){

        Image imageTitle;

        try {
            FileInputStream inputStreamTitle = new FileInputStream(path);
            imageTitle = new Image(inputStreamTitle);
            return imageTitle;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.exit(1);
        }


        return null;
    }

}
