package en.awacademy.game;

public class Square {

    private double posX;
    private double posY;
    private String color;
    private String soundPath;

    public Square(double posX, double posY, String color, String soundPath) {
        this.posX = posX;
        this.posY = posY;
        this.color = color;
        this.soundPath = soundPath;
    }

    public Square(String color) {
        this.color = color;
    }

    public Square() {}

    public double getPosX() {
        return posX;
    }

    public double getPosY() {
        return posY;
    }

    public String getColor() {
        return color;
    }

    public String getSoundPath() {
        return soundPath;
    }
}
