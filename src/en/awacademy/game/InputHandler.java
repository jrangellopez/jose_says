package en.awacademy.game;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;

import java.util.Arrays;

public class InputHandler {

    private Model model;
    private GraphicsContext gc;


    public InputHandler(Model model) {
        this.model = model;
    }


    public void onClick(MouseEvent event) {

        //to start the program. It goes from phase 0 to phase 1
        if (event.getX() >= 400 && event.getX() <= 450
                && event.getY() >= 400 && event.getY() <= 450) {
            this.model.newRandomSquare();

            //show serie
            this.model.startShow();


            this.model.correctString = "";
            System.out.println("Size of random squares: " + this.model.getRandomSquares().size());
            System.out.println(Arrays.toString(this.model.getRandomSquares().toArray()));
            System.out.println(this.model.getUserSquares());
            System.out.println(this.model.correctCounter);
        }

        if(this.model.getPhase() == 2){
            if (event.getX() >= 150 && event.getX() < 250 && event.getY() >= 150 && event.getY() < 250) {
                this.model.newUserSquare("#e39c96");//red

            } else if (event.getX() >= 150 && event.getX() < 250 && event.getY() >= 250 && event.getY() < 350) {
                this.model.newUserSquare("#9cbae8");//blue

            } else if (event.getX() >= 250 && event.getX() < 350 && event.getY() >= 150 && event.getY() < 250) {
                this.model.newUserSquare("#9ae3c5");//green

            } else if (event.getX() >= 250 && event.getX() < 350 && event.getY() >= 250 && event.getY() < 350) {
                this.model.newUserSquare("#d9dbb8");//yellow

            }
        }



        if (this.model.getPhase() == 2) {
            System.out.println("Size of user squares: " + this.model.getUserSquares().size());


            if (this.model.getUserSquares().size() == this.model.getRandomSquares().size()) {
                this.model.countCorrectSquares();
                this.model.compare();
                System.out.println(this.model.correctString);

                if (this.model.correctCounter == this.model.getRandomSquares().size()) {
                    this.model.reset();
                }
            }
        }

        if(this.model.correctString.equals("Incorrect")){

            this.model.getRandomSquares().clear();
            this.model.getUserSquares().clear();
            this.model.setCorrectCounter(0);
        }


    }



}
